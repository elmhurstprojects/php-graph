# PHP GraphQL Builder

This is a PHP version of my Javascript GraphQL query Builder.
Intention of use is to be able to do endpoint tests to check
models, security, validation and GraphQL setup.

### TODO Not done the cURL headers to cater for auth yet!!!!

### Sample Usage

Build the Request

    $data = $this->graphQueryRequest->endpoint('user')
        ->params(['id' => 2])
        ->fields(['meta', 'active', 'type'])
        ->relations([
            [
            'relation' => 'testRelation',
            'fields' => ['id', 'name']
            ]
        ])
        ->orderBy([(object)['field' => 'CREATED_AT', 'order' => 'DESC']])
        ->paginate(10, 1);

Send the request

    $this->graph->setup('http://localhost/graphql')
        ->getEndPointQuery($data)