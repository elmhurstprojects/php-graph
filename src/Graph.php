<?php

namespace ElmhurstProjects\PHPGraph;

use ElmhurstProjects\PHPGraph\QueryStringBuilder;

class Graph{

    protected $queryStringBuilder;

    protected $url;

    public function __construct()
    {
        $this->queryStringBuilder = new QueryStringBuilder();
    }

    /**
     * Sets up request such as url and auth
     * @param $url
     * @return Graph
     */
    public function setup(string $url): Graph
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Return data object as a string
     * @param GraphQueryRequest $graphQueryRequest
     * @return string
     */
    public function queryToString(GraphQueryRequest $graphQueryRequest):string
    {
        return $this->queryStringBuilder->buildQueryString($graphQueryRequest);
    }

    public function getEndPointQuery(GraphQueryRequest $graphQueryRequest)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$this->url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            'query='.$this->queryStringBuilder->buildQueryString($graphQueryRequest));

        $response = curl_exec($ch);

        curl_close ($ch);

        return $response;
    }
}