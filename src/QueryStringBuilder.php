<?php

namespace ElmhurstProjects\PHPGraph;

class QueryStringBuilder
{
    protected $total_params = 0;

    /**
     * Entry method into class
     * @param GraphQueryRequest $graphQueryRequest
     * @return string
     */
    public function buildQueryString(GraphQueryRequest $graphQueryRequest): string
    {
        $paginate = false;

        $query = $graphQueryRequest->getEndpoint();

        if ($graphQueryRequest->getPaginate()) {
            // add paginator params to the main params object
            $graphQueryRequest->addParam('first', $graphQueryRequest->getPaginate()->first)
                ->addParam('page', $graphQueryRequest->getPaginate()->page);

            $paginate = '{paginatorInfo{total,count,currentPage,firstItem,hasMorePages,lastItem,lastPage,perPage},';
        }

        $query .= $this->addQueryParams($graphQueryRequest);

        // add paginator return fields if set (this var will be null otherwise)
        if ($paginate !== false) $query .= $paginate;

        if ($graphQueryRequest->getPaginate()) $query .= 'data';

        if ($graphQueryRequest->getFields()) {
            $query .= '{' . $this->fieldsToString($graphQueryRequest->getFields());

            if ($graphQueryRequest->getRelations()) {
                $query .= ',' . $this->relationsToString($graphQueryRequest->getRelations());
            }

            $query .= '}';
        }

        if ($graphQueryRequest->getFieldsRaw()) {
            $query .= $graphQueryRequest->getFieldsRaw();
        }

        if ($graphQueryRequest->getPaginate()) $query .= '}';

        return   '{' . $query . '}';;
    }

    /**
     * Add the query parameters
     * @param GraphQueryRequest $graphQueryRequest
     * @return string
     */
    protected function addQueryParams(GraphQueryRequest $graphQueryRequest): string
    {
        if ($graphQueryRequest->getParamsRaw()) return $this->addQueryRawParams($graphQueryRequest);

        $param_string = '(';

        if ($graphQueryRequest->getParams()) {
            $param_string .= $this->paramsToString($graphQueryRequest->getParams());
        }

        if ($graphQueryRequest->getOrderBy()) {
            if ($this->total_params > 0) $param_string .= ',';

            $param_string .= 'orderBy: [';

            foreach ($graphQueryRequest->getOrderBy() as $order_by) {
                $param_string .= '{field:' . $order_by->field . ', order:' . $order_by->order . '}';
            }

            $param_string .= ']';
        }

        $param_string .= ')';

        if ($param_string === '()') return '';

        return $param_string;
    }

    /**
     * Add raw query parameters
     * @param GraphQueryRequest $graphQueryRequest
     * @return string
     */
    protected function addQueryRawParams(GraphQueryRequest $graphQueryRequest): string
    {
        if ($graphQueryRequest->getParamsRaw() && $graphQueryRequest->getPaginate()) {
            return '(' . $this->paramsToString($graphQueryRequest->getParams()) . $graphQueryRequest->getParamsRaw() . ')';
        } else {
            return '(' . $graphQueryRequest->getParamsRaw() . ')';
        }
    }

    /**
     * Turns parameters into string
     * @param $params
     * @return string
     */
    protected function paramsToString($params): string
    {
        $paramString = '';

        foreach ($params as $key => $value) {
            $this->total_params++;

            if ($value !== null) {
                if (is_array($value)) {
                    $newValue = '';

                    foreach ($value as $v) {
                        $newValue = '"' . $v . '",';
                    }

                    $paramString .= $key . ':[' . $newValue . '],';
                } else {
                    switch (getType($value)) {
                        case "string":
                        case "integer":
                        case "boolean":
                            $paramString .= $key . ':' . $value . ',';
                            break;

                        case "array":
                            $paramString .= $key . ':[' . $this->paramsToString($value) . '],';
                            break;

                        case "object":
                            $paramString .= $key . ':{' . $this->paramsToString($value) . '},';
                            break;
                    }
                }
            }
        }

        return substr($paramString, 0, -1);
    }

    /**
     * Turn requested fields into string
     * @param array $fieldArray
     * @return false|string
     */
    protected function fieldsToString(array $fieldArray)
    {
        $fieldString = '';

        foreach ($fieldArray as $field) {
            if (is_string($field)) $fieldString .= $field . ',';
        }

        return substr($fieldString, 0, -1);
    }

    /**
     * Turn the relationships into string
     * @param array $relations
     * @return string
     */
    protected function relationsToString(array $relations): string
    {
        $relationString = '';

        foreach ($relations as $key => $value) {
            $relationString .= $value->relation . '{';

            $relationString .= $this->fieldsToString($value->fields);

            if (isset($value->relations)) {
                $relationString .= ' ' . $this->relationsToString($value->relations);
            }

            $relationString .= '},';
        }

        return substr($relationString, 0, -1);
    }
}