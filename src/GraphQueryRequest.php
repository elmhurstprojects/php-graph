<?php

namespace ElmhurstProjects\PHPGraph;

class GraphQueryRequest{

    protected $endpoint;

    protected $fields;

    protected $fields_raw;

    protected $order_by;

    protected $paginate;

    protected $relations;

    protected $params;

    protected $params_raw;

    public function endpoint(string $endpoint): GraphQueryRequest
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    public function params(array $params): GraphQueryRequest
    {
        $this->params = (object)$params;

        return $this;
    }

    public function addParam(string $field, string $value): GraphQueryRequest
    {
        if(!isset($this->params)) $this->params = (object)[];

        $this->params->$field = $value;

        return $this;
    }

    public function paramsRaw(string $params_raw): GraphQueryRequest
    {
        $this->params_raw = $params_raw;

        return $this;
    }

    public function fields(array $fields): GraphQueryRequest
    {
        $this->fields = $fields;

        return $this;
    }

    public function fieldsRaw(string $fields_raw): GraphQueryRequest
    {
        $this->fields_raw = $fields_raw;

        return $this;
    }

    public function orderBy(array $order_by): GraphQueryRequest
    {
        $this->order_by = $order_by;

        return $this;
    }

    public function paginate(int $first, int $page): GraphQueryRequest
    {
        $this->paginate = (object)['first' => $first, 'page' => $page];

        return $this;
    }

    public function relations(array $relations): GraphQueryRequest
    {
        if(!isset($this->relations)) $this->relations = [];

        foreach($relations as $relation){
            $this->relations[] = (object)['relation' => $relation['relation'], 'fields' => $relation['fields']];
        }

        return $this;
    }

    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    public function getParams(): ?\stdClass
    {
        return $this->params;
    }

    public function getParamsRaw(): ?string
    {
        return $this->params_raw;
    }

    public function getFields(): ?array
    {
        return $this->fields;
    }

    public function getFieldsRaw(): ?string
    {
        return $this->fields_raw;
    }

    public function getOrderBy(): ?array
    {
        return $this->order_by;
    }

    public function getPaginate(): ?\stdClass
    {
        return $this->paginate;
    }

    public function getRelations(): ?array
    {
        return $this->relations;
    }


}